#pragma once

#include <quickfix/Application.h>
#include <quickfix/Message.h>
#include <quickfix/MessageCracker.h>
#include <quickfix/Session.h>
#include <quickfix/SessionID.h>
#include "quickfix/Utility.h"
#include "quickfix/Values.h"

#include <future>
#include <mutex>

using namespace FIX;

class TestApp : public Application, public MessageCracker {
public:
	TestApp(const std::map<std::string, std::pair<std::string, std::string>>& _logins) : order_id(0), exec_id(0), sub_id(0), logins(_logins) {
		for (const auto& pair : logins) {
			loggedInSessions[pair.first] = false;
		}
	};
	~TestApp() = default;
	void onCreate(const SessionID&) override;
	void onLogon(const SessionID&) override;
	void onLogout(const SessionID&) override;
	void toAdmin(Message&, const SessionID&) override;
	void toApp(Message&, const SessionID&) throw(FIX::DoNotSend) override;
	void fromAdmin(const Message&, const SessionID&) throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon) override;
	void fromApp(const Message&, const SessionID&) throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType) override;
	void onMessage(const FIX44::MarketDataSnapshotFullRefresh& message, const SessionID& session_id) throw(FIX::FieldNotFound&, FIX::IncorrectDataFormat&, FIX::IncorrectTagValue&, FIX::UnsupportedMessageType&) override;
	void subscribe_session(const std::string& symbol, const SessionID& session_id);
	void subscribe(const std::string& symbol);
	bool isLoggedIn(const std::string& senderCompId) {
		return loggedInSessions[senderCompId];
	};
	bool isLoggedIn() {
		for (const auto& pair : loggedInSessions) {
			if (!pair.second) {
				return false;
			}
		}
		return true;
	};
	void run();
	bool getPrices(const std::string& symbol, double* spread, double* ask);
	bool setAccount(const std::string& account);

	std::string genOrderID() {
		std::stringstream stream;
		stream << ++order_id;
		return stream.str();
	}
	std::string genExecID() {
		std::stringstream stream;
		stream << ++exec_id;
		return stream.str();
	}
	std::string genSubID() {
		std::stringstream stream;
		stream << ++sub_id;
		return stream.str();
	}

private:
	int order_id, exec_id, sub_id;
	std::map<std::string, std::pair<std::string, std::string>> logins;
	std::map < std::string, std::map<std::string, std::pair<double, double>>> currentPrices;
	std::string subscribing_symbol;
	std::string subscribing_account;
	std::mutex subscribing_mutex;
	bool subscribing = false;
	std::condition_variable subscribecv;
	std::map<std::string, std::string> symbolPrefixBrokerMap;
	std::map<std::string, bool> loggedInSessions;
	std::map<std::string, FIX::SessionID> compIdToSession;
	std::vector<std::string> currentAssets;
	std::string currentAccount;
};

typedef double DATE;
typedef struct T6
{
	DATE	time;
	float fHigh, fLow;	// (f1,f2)
	float fOpen, fClose;	// (f3,f4)	
	float fVal, fVol; // optional data, like spread and volume (f5,f6)
} T6; // 6-stream tick, .t6 file content

extern "C" __declspec(dllexport) int BrokerOpen(char* Name, FARPROC fpError, FARPROC fpProgress);
extern "C" __declspec(dllexport) int BrokerLogin(char* User, char* Pwd, char* Type, char* Accounts);
extern "C" __declspec(dllexport) int BrokerTime(DATE *pTimeUTC);
extern "C" __declspec(dllexport) int BrokerAsset(char* Asset, double *pPrice, double *pSpread, double *pVolume, double *pPip, double *pPipCost, double *pLotAmount, double *pMarginCost, double *pRollLong, double *pRollShort);
extern "C" __declspec(dllexport) int BrokerAccount(char* Account, double *pBalance, double *pTradeVal, double *pMarginVal);
extern "C" __declspec(dllexport) int BrokerBuy(char* Asset, int nAmount, double dStopDist, double *pPrice) { return 0; };
extern "C" __declspec(dllexport) int BrokerHistory2(char* Asset, DATE tStart, DATE tEnd, int nTickMinutes, int nTicks, T6* ticks) { return 0; };
extern "C" __declspec(dllexport) int BrokerTrade(int nTradeID, double *pOpen, double *pClose, double *pRoll, double *pProfit) { return 0; }
extern "C" __declspec(dllexport) int BrokerStop(int nTradeID, double dStop) { return 0; }
extern "C" __declspec(dllexport) int BrokerSell(int nTradeID, int nAmount) { return 0; }